/*
Peregrine a basic bot for DiscordApp which posts Tweets from a selected Twitter account in a selected DiscordApp channel.

Copyright (C) <2015-2017>  <Peter Stevenson (2E0PGS)>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// --------------Variables--------------
var discord = require("discord.js"); // Library for Discord API.
var twitter = require('twitter'); // Library for Twitter API.
var multiline = require("multiline"); // Used so we can have nice multiline !help responce.

var configFile = require("./config.json"); // Our config file.
var discordChannelID = configFile.discordChannelID; // Store channel ID we want to post tweets to.
var twitterAccountID = configFile.twitterAccountID; // Store the Twitter account ID we want to track.

var discordClient = new discord.Client();

var twitterClient = new twitter({
	consumer_key: configFile.twitterConsumerKey,
	consumer_secret: configFile.twitterConsumerSecret,
	access_token_key: configFile.twitterAccessTokenKey,
	access_token_secret: configFile.twitterAccessTokenSecret
});
// --------------Variables-----------END

// Twitter tracking.
twitterClient.stream('statuses/filter', {follow: twitterAccountID},  function(stream){

	stream.on('data', function(tweet) { // Stream on new tweet.
	    if (tweet['retweeted'] === false && tweet.text.indexOf("@") === -1 && tweet.text.indexOf("RT") === -1){
		    console.log("New tweet from tracker. Tweet matches ID of configFile.twitterAccountID: " + tweet.text);
	        discordClient.channels.get(discordChannelID).send(tweet.text); // Send tweet to discord.
	    }
	});

	stream.on('error', function(error) {
		console.log(error);
		console.log("Twitter Disconnected!");
		process.exit(1); // Exit node.js with an error.
	});

});
// End of Twitter tracking.

discordClient.login(configFile.discordToken); // Auth with discord servers.

discordClient.on("ready", function() {
	console.log("Connected...! Peregrine is now connected to " + discordClient.guilds.size + " Discord app servers, and " + discordClient.channels.size + " channels.");
	discordClient.user.setGame("Twitter");
});

discordClient.on("disconnected", function() {
	var date = new Date();
	console.log((date) + " Peregrine has been Disconnected!");
	process.exit(1); // Exit node.js with an error.
});

discordClient.on("message", function(msg) {
	if (msg.content[0] === '!') {
		var command = msg.content.toLowerCase().split(" ")[0].substring(1);
		var suffix = msg.content.toLowerCase().substring(command.length + 2);
		var cmd = commands[command];
		if (cmd) {
			cmd.process(discordClient, msg, suffix);
		}
		console.log(msg.author.username + " (" + msg.author.id + ") " + "Used: " + command) //log user ID + Username + Command
	}
});

// Here is our command list. Add commands here.
var commands = {
	"ping": {
        description: "Useful for checking if the bot is alive.",
        process: function(discordClient, msg, suffix) {
            discordClient.channels.get(msg.channel.id).send("Pong!");
        }
	},
	"help": {
		process: function(discordClient, msg, suffix) {
			discordClient.channels.get(msg.channel.id).send(help);
		}
	},
	"commands": {
		process: function(discordClient, msg, suffix) {
			discordClient.channels.get(msg.channel.id).send(help);
		}
	},
	"about": {
		process: function(discordClient, msg, suffix) {
			discordClient.channels.get(msg.channel.id).send(about);
		}
	},
	"twitter": {
		process: function(discordClient, msg, suffix) {
			discordClient.channels.get(msg.channel.id).send(twitterUrlList);
		}
	}
}; // End of command list.

var help = multiline(function() {/*
```
Help function:

!ping    Checks if bot is responsive.
!help    Lists avalible commands.
!about   Lists info about bot.
!twitter Lists tracked twitter account.
```
*/});

var about = multiline(function() {/*
```
A simple bot with a few core functions.
Main function is to post twitter feed into channel chat.
Written by: @2E0PGS
Licence: GNU GPL v3.
```
*/});

var twitterUrlList = multiline(function() {/*
```
Twitter Accounts Being Tracked:
https://twitter.com/discordapp
```
*/});
