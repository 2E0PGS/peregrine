# README #

![3644850654-7-peregrine-logo_avatar.png](https://bitbucket.org/repo/appEMA/images/2511827325-3644850654-7-peregrine-logo_avatar.png)

### What is Peregrine?
Peregrine is a simple bot for DiscordApp the new gamer chat program that replaces Skype and TeamSpeak.
This bot is designed to post Twitter Tweets from a feed right into the DiscordApp chat.
It is designed to work with twitter users how ever it can work with keywords or more ( with a little tinkering ).

Feel free to fork this. I hope to make other bots. I will try to keep them separate that way people can choose what they need.
Bots can always be merged together to perform several tasks.

Version: 2.0.0

### How do I get set up? ###

* Clone this repo: `git clone https://2E0PGS@bitbucket.org/2E0PGS/peregrine.git`
* Edit `config.example` and add Discord channel ID, Discord API token, Twitter account ID and Twitter API tokens. Then save it as `config.json`
* Install NodeJS version 6 or newer.
* Install npm (Node Package Manager).
* Run npm update in the source folder: `npm update`
* Linux: `./start-peregrine.sh` Windows: `start-peregrine.bat`

### Licence? ###

```
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
```


##### Photo source? #####
*Peregrine Falcon
https://www.flickr.com/photos/66164549@N00/2314217251
By: Keven Law*
